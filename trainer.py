from data import get_cifar10_data
from model import SimpleClassifierModel
from callbacks import defined_callbacks
import tensorflow as tf

def train(config):

    model = SimpleClassifierModel()()
    data_train, data_test = get_cifar10_data()

    data_train = data_train.batch(config['batch_size'])
    data_test = data_test.batch(config['batch_size'])

    history = model.fit(data_train, epochs=config['epochs'], validation_data=data_test, callbacks=defined_callbacks(config))
    model.evaluate(data_test)
