import tensorflow as tf
from tensorflow.keras.utils import to_categorical

def get_cifar10_data():
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
    x_train = x_train / 255.
    x_test = x_test / 255.
    y_train = to_categorical(y_train, 10)
    y_test = to_categorical(y_test, 10)
    data_train = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    data_test = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    return data_train, data_test
